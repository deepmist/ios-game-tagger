//
//  GLAppDelegate.h
//  GLGameLists
//
//  Created by Joe Lagomarsino on 2/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
