//
//  GLFirstViewController.h
//  GLGameLists
//
//  Created by Joe Lagomarsino on 2/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLReleaseListViewController : UITableViewController
{
    NSInteger day;
    NSInteger month;
    NSInteger quarter;
    NSInteger year;
}

@property (nonatomic, strong) NSArray *releases;
@property (nonatomic, strong) NSDateFormatter *dateInterpreter;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end
