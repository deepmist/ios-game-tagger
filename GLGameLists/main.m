//
//  main.m
//  GLGameLists
//
//  Created by Joe Lagomarsino on 2/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GLAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GLAppDelegate class]));
    }
}
