//
//  GLGameCell.m
//  GLGameLists
//
//  Created by Joe Lagomarsino on 4/19/13.
//  Copyright (c) 2013 Home. All rights reserved.
//

#import "GLGameCell.h"

@implementation GLGameCell

+ (GLGameCell *)getCell
{
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GLGameCell" owner:self options:nil];
    GLGameCell *cell = [topLevelObjects objectAtIndex:0];
    return cell;
}

- (void)setGame:(NSDictionary *)gameDictionary
{
    NSString* name = ((NSNull*)[gameDictionary objectForKey:@"name"] != [NSNull null]) ? [gameDictionary objectForKey:@"name"] : @"ERROR";
	self.nameLabel.text = name;
    self.dateLabel.text = ((NSNull*)[gameDictionary objectForKey:@"expected_release_day"] != [NSNull null]) ? [gameDictionary objectForKey:@"expected_release_day"] : @"tba";
    
    self.systemsLabel.text = @"";
    NSArray *platforms = [gameDictionary objectForKey:@"platforms"];
    for (int i = 0; i < [platforms count]; i++)
    {
        self.systemsLabel.text = [NSString stringWithFormat:@"%@%@%@",
                                  self.systemsLabel.text,
                                  [platforms objectAtIndex:i],
                                  (i + 1 == [platforms count]) ? @"" : @", "];
    }
}

@end
