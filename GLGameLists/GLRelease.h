//
//  GLRelease.h
//  GLGameLists
//
//  Created by Joe Lagomarsino on 2/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLRelease : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *date;

@end
