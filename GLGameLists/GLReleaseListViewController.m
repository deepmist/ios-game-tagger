//
//  GLFirstViewController.m
//  GLGameLists
//
//  Created by Joe Lagomarsino on 2/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GLReleaseListViewController.h"

#import "GLRelease.h"
#import "DSActivityView.h"
#import "GLGameCell.h"
#import "PKRevealController.h"

@implementation GLReleaseListViewController

@synthesize releases=_releases;

- (id)init
{
    self = [super init];
    if (self) {
        [self grabData];
    }
    return self;
}

- (void)grabData
{
	NSError* error = nil;
	NSURLResponse* response = nil;
	NSMutableURLRequest* request = [[[NSMutableURLRequest alloc] init] autorelease];
	//NSString* searchURL = [NSString stringWithFormat:@"http://localhost/api.gamelist/v0/releases/%d/%d", year, month];
	NSString* searchURL = [NSString stringWithFormat:@"http://joelagomarsino.com/gametag/v0/releases/%d/%d", year, month];
    
	NSURL* URL = [NSURL URLWithString:searchURL];
	[request setURL:URL];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
	[request setTimeoutInterval:30];
	
	NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	if (error)
	{
		NSLog(@"Error performing request %@", searchURL);
		return;
	}
    
	NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSData *releasesData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *releasesDictionary = [NSJSONSerialization JSONObjectWithData:releasesData options:0 error:nil];
	self.releases = [releasesDictionary objectForKey:@"release"];
    
    [self performSelectorOnMainThread:@selector(refresh) withObject:nil waitUntilDone:TRUE];
}

- (void)refresh
{
    self.navigationItem.title = [NSString stringWithFormat:@"%d/%d", month, year];
    [self.tableView reloadData];
    [DSBezelActivityView removeViewAnimated:TRUE];
}

- (void)updateReleases
{
    [DSBezelActivityView activityViewForView:self.view withLabel:@"Loading..."];
    [self performSelector:@selector(grabData) withObject:nil afterDelay:0.0];
}

- (void)loadView
{
    [super loadView];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"reveal_menu_icon_portrait.png"]
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(menuPressed)];
    self.navigationItem.leftBarButtonItem = backButton;
    [backButton release];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithImage:[UIImage imageNamed:@"icon_arrow_right.png"]
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(nextPressed)];
    self.navigationItem.rightBarButtonItem = rightButton;
    [rightButton release];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"MM-dd-yyyy"];
    self.dateInterpreter = [[NSDateFormatter alloc] init];
    [self.dateInterpreter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSDateComponents* components = [[NSCalendar currentCalendar]
                                    components: NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                    fromDate: [NSDate date]];
    day = [components day];
    month = [components month];
    quarter = -1;
    year = [components year];
    
    [self updateReleases];
}

- (void)menuPressed
{
    if (self.navigationController.revealController.focusedController == self.navigationController.revealController.leftViewController)
    {
        [self.navigationController.revealController showViewController:self.navigationController.revealController.frontViewController];
    }
    else
    {
        [self.navigationController.revealController showViewController:self.navigationController.revealController.leftViewController];
    }
}

- (void)nextPressed
{
    day = 0;
    
    month++;
    if (month > 12)
    {
        month = 1;
        year++;
    }
    
    [self updateReleases];
}

- (void)prevPressed
{
    day = 0;
    
    month--;
    if (month < 1)
    {
        month = 12;
        year--;
    }
    
    [self updateReleases];
}

#pragma mark - TableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.releases count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = @"ReleaseCell";
	GLGameCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil)
        cell = [GLGameCell getCell];
    
    [cell setGame:[self.releases objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - Cleanup

-(void)dealloc
{
    [super dealloc];
}

@end
