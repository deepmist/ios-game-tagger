//
//  GLGameCell.h
//  GLGameLists
//
//  Created by Joe Lagomarsino on 4/19/13.
//  Copyright (c) 2013 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLGameCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) IBOutlet UILabel *systemsLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateLabel;

+ (GLGameCell *)getCell;
- (void)setGame:(NSDictionary *)gameDictionary;

@end
